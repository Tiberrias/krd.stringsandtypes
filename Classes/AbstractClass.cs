﻿namespace Classes
{
  public abstract class AbstractClass
  {
    private readonly string _data;

    protected AbstractClass(string data)
    {
      _data = data;
    }

    public virtual string GetData()
    {
      return _data;
    }
  }

  public class DerivedClass : AbstractClass
  {
    public DerivedClass(string data) : base(data)
    {
    }

    public override string GetData()
    {
      return base.GetData() + " and more Data!";
    }
  }
}
