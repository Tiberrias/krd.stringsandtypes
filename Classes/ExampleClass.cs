﻿namespace Classes
{
  public class ExampleClass
  {
    protected const int Constant = 1;

    private int _field;

    public int Property
    {
      get { return _field; }
      set { _field = value; }
    }

    public static string StaticProperty { get; set; }

    private void Method(string methodParameter) { }

    private void Method(int overloadedMethodParameter) { }
  }
}