﻿using System;
using ParserExample;

namespace ClassesExample
{
  public class InputDataProcessor
  {
    private readonly ICsvRowParser _csvRowParser;
    private readonly VeryImportantBusinessEntityRepository _repository;

    public InputDataProcessor()
    {
      _csvRowParser = new CsvRowParser();
      _repository = new VeryImportantBusinessEntityRepository();
    }

    public string GetProcessorMessage()
    {
      return "Processing very important business entities!";
    }

    public void ProcessData(string data)
    {
      if (String.IsNullOrEmpty(data))
      {
        throw new ArgumentException();
      }

      foreach (string row in data.Split('\n'))
      {
        _repository.Save(_csvRowParser.Parse(row));
      }
    }
  }
}