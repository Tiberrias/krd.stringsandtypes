﻿using System;

namespace ClassesExample
{
  class Program
  {
    static void Main(string[] args)
    {
      InputDataProcessor processor = new InputDataProcessor();

      processor.ProcessData("");
      Console.WriteLine(processor.GetProcessorMessage());
    }
  }
}
