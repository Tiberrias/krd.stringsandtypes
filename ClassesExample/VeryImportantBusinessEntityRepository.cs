﻿using ParserExample;

namespace ClassesExample
{
  public class VeryImportantBusinessEntityRepository
  {
    public void Save(VeryImportantBusinessEntity entity)
    {
      //Some logic would be here
    }
  }
}