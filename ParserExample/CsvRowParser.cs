﻿using System;
using System.Globalization;
using System.Text;

namespace ParserExample
{
  public class CsvRowParser : ICsvRowParser
  {
    public VeryImportantBusinessEntity Parse(string csvRow)
    {
      var result = new VeryImportantBusinessEntity();

      if (String.IsNullOrWhiteSpace(csvRow))
      {
        return null;
      }

      var splittedData = csvRow.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

      if (splittedData.Length < 6)
      {
        return null;
      }

      result.Id = splittedData[0].ToUpper().Trim();
      result.FullName = CultureInfo.CurrentCulture.TextInfo.ToTitleCase($"{splittedData[1]} {splittedData[2]}");
      result.OrderId = Int32.Parse(splittedData[3].Trim('"'));
      result.OrderDate = DateTime.Parse(splittedData[4]);
      result.Amount = Decimal.Parse(splittedData[5]);

      if (splittedData.Length > 6)
      {
        var stringBuilder = new StringBuilder();
        result.Notes = String.Empty;
        for (int i = 6; i < splittedData.Length; i++)
        {
          stringBuilder.AppendLine(splittedData[i]);
        }

        result.Notes = stringBuilder.ToString();
      }

      return result;
    }
  }
}