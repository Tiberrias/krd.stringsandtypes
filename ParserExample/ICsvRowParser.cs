﻿namespace ParserExample
{
  public interface ICsvRowParser
  {
    VeryImportantBusinessEntity Parse(string csvRow);
  }
}