﻿using FluentAssertions;
using NUnit.Framework;
using System;

namespace ParserExample
{
    [TestFixture]
    public class ParserTests
    {
        [Test]
        public void Parse_ParsesExpectedResult()
        {
            var exampleCsvRow = @"Xyz/a/01 ;jan;kowalski;""15"";01.10.2017;123,55;;notatka;druga notatka;;kolejna notatka;;;jeszcze jedna notatka";
            var expectedResult = new VeryImportantBusinessEntity
            {
                Id = "XYZ/A/01",
                FullName = "Jan Kowalski",
                OrderId = 15,
                OrderDate = new DateTime(2017, 10, 1),
                Amount = 123.55m,
                Notes = "notatka\r\ndruga notatka\r\nkolejna notatka\r\njeszcze jedna notatka\r\n"
            };

            var parser = new CsvRowParser();

            var result = parser.Parse(exampleCsvRow);

            result.ShouldBeEquivalentTo(expectedResult);
        }

    }
}