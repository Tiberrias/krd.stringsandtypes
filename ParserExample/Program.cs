﻿using System;

namespace ParserExample
{
  class Program
  {
    static void Main(string[] args)
    {
      var exampleCsvRow = @"Xyz/a/01  ;jan;kowalski;""15"";01.21.2017;123,55;;notatka;druga notatka;;kolejna notatka;;;jeszcze jedna notatka";

      ICsvRowParser parser = new CsvRowParser();

      VeryImportantBusinessEntity result = parser.Parse(exampleCsvRow);

      Console.ReadKey();
    }
  }
}
