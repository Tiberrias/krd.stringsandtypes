﻿using System;

namespace ParserExample
{
    public class VeryImportantBusinessEntity
    {
        /// <summary>
        /// Should be uppercase string.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Should start with uppercase letter
        /// </summary>
        public string FullName { get; set; }

        public int OrderId { get; set; }

        public DateTime OrderDate { get; set; }

        public decimal Amount { get; set; }

        /// <summary>
        /// Each note in new line
        /// </summary>
        public string Notes { get; set; }
    }
}