﻿using System;


namespace ExHandling
{
  class Program
  {
    static void Main(string[] args)
    {

      Collection tab = new Collection();

      try
      {
        int value = tab.getElement(20);
        Console.WriteLine("Element nr 20 ma wartość: " + value);
      }
      catch (ElementNotFoundException)
      {
        Console.WriteLine("Wyjątek ElementNotFoundException");
      }

      catch (IndexOutOfRangeException)
      {
        Console.WriteLine("Wyjątek SystemException");
      }
      catch (SystemException)
      {
        Console.WriteLine("Wyjątek IndexOutOfRangeException");
      }

      Console.ReadKey();
    }
  }

  public class Collection
  {
    int[] tab;

    public Collection()
    {
      tab = new int[5];
    }

    public int getElement(int i)
    {
      if (i < 0 || i > tab.Length - 1)
      {
        throw new ElementNotFoundException();
      }
      else
      {
        return tab[i];
      }
    }
  }

  public class ElementNotFoundException : Exception
  {
    public ElementNotFoundException()
    {

    }

    public ElementNotFoundException(string message)
      : base(message)
    {

    }

    public ElementNotFoundException(string message, Exception innerException)
      : base(message, innerException)
    {

    }
  }
}
