﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IandAbstractEx
{
  internal class Program
  {
    private static void Main(string[] args)
    {

      char c;

      int i;


      float f;

      double d;

      decimal dec;
    }

    private interface ISampleInterface
    {
      string SampleMethod();
    }

    private class MySampleClass :ISampleInterface
    {
      public string SampleMethod()
      {
        return "Hello I'm MySampleClass";
      }
    }

    private abstract class ShapesClass

    {
      protected int _dimensionX;

      public int GetXdimension()
      {
        return _dimensionX;
      }

      abstract public int Area(); 
    }

    private class Rectangular : ShapesClass
    {
      private int _dimensionY;

      public override int Area()
      {
        return _dimensionX*_dimensionY;
      }
    }
  }
}
