﻿
namespace NestedProtectedCheck
{
  class Program
  {
    static void Main(string[] args)
    {
      Customer cus = new Customer();

//      Customer.MyCustomer 
    }

    public class Customer
    {
      protected class MyCustomer
      {
         
      }
    }

    class CorpoCustomer : Customer
    {
      public void MyMethod()
      {
        MyCustomer myCustomer = new MyCustomer();

       string result = myCustomer.ToString();
      }
    }
  }
}
