﻿using System;

namespace PolymorphismExample
{
  class Program
  {
    static void Main()
    {
      Employee em = new Employee();
      Console.WriteLine("Employee 1 responsibilities: {0}", em.Responsibilities());

      em = new Manager();
      Console.WriteLine("Employee 2 responsibilities: {0}", em.Responsibilities());
      Console.ReadKey();

      Employee em2 = new Employee();
      Console.WriteLine("Employee 1 department: {0}", em2.Department());

      em2 = new Manager();
      Console.WriteLine("Employee 2 department: {0}", em2.Department());
      Console.ReadKey();
    }

    public class Manager : Employee
    {
      public Employee[] subordinates { get; set; }

      public override string Responsibilities()
      {
        return "Management";
      }

      public new string Department()
      {
        return "Business department.";
      }
    }

    public class Employee
    {
      public string Name { get; set; }
      public DateTime DateofBirth { get; set; }
      public ushort Age()
      {
        return (ushort)(DateTime.Now.Year - this.DateofBirth.Year);
      }

      /*...*/

      /// <summary>
      /// 
      /// </summary>
      /// <exception cref="">Potencjalny exception</exception>
      /// <returns></returns>
      public virtual string Responsibilities()
      {
        return "Code development";
      }

      public string Department()
      {
        return "IT department.";
      }
    }
  }
}
