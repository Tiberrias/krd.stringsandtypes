﻿using System;

namespace SimplestApplication
{
  class Program
  {
    static void Main()
    {
      Console.WriteLine("Podaj swoje imie:");

      string imie = Console.ReadLine();

      Console.WriteLine("Twoje imie to: " + imie);

      Console.WriteLine("Wcisnij dowolny klawisz by zakonczyc program.");

      Console.ReadKey(); 
    }
  }
}
