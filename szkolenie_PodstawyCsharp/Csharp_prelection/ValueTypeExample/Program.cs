﻿using System;

namespace ValueTypeExample
{
  class Program
  {
    static void Main(string[] args)
    {
      int num1, num2;
      num1 = 5;
      num2 = num1;
      Console.WriteLine("num1 is {0}. num2 is {1}", num1, num2);

      num2 = 3;
      Console.WriteLine("num1 is {0}. num2 is {1}", num1, num2);
      Console.ReadLine();
      return;
    }
  }
}
