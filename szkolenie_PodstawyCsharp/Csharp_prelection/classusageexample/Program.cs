﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassUsageExample
{
  class Program
  {
    static void Main(string[] args)
    {
      Customer ourCustomer;

      ourCustomer = new Customer();

      ourCustomer.DateOfBirth = new DateTime(1970, 04,10);
      ourCustomer.FirstName = "Zbigniew";
      ourCustomer.LastName = "Iksiński";

      string result = ourCustomer.ToString();

      Console.WriteLine(result);
      Console.ReadKey();

    }
  }

  public class Customer
  {
    //Fields
    public DateTime DateOfBirth;

    //Properties
    internal string FirstName { get; set; }

    public string LastName { get; set; }

    //Methods
    public override string ToString()
    {
      return string.Format(
      "First name: {0}, Last name: {1}, Date of birth: {2}",
            FirstName, LastName, DateOfBirth
      );
    }
  }
}
