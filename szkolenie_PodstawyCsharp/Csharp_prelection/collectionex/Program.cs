﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionEx
{
  class Program
  {
    static void Main(string[] args)
    {

      SortedList<string, int> sorted = new SortedList<string, int>();
      sorted.Add("perls", 3);
      sorted.Add("dot", 1);
      sorted.Add("net", 2);

      //
      // Test SortedList with ContainsKey method.
      //
      bool contains1 = sorted.ContainsKey("java");
      Console.WriteLine("contains java = " + contains1);

      //
      // Use TryGetValue method.
      //
      int value;
      if (sorted.TryGetValue("perls", out value))
      {
        Console.WriteLine("perls key is = " + value);
      }

      foreach (var pair in sorted)
      {
        Console.WriteLine(pair);
      }
      Console.ReadKey();
    }
  }
}
