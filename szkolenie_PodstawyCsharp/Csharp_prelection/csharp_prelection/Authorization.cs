﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Csharp_prelection
{
  public class Authorization
  {
    public virtual bool Authorize(string userName, string resource)
    {
      bool isAuthorized = false;

      /*...*/

      return isAuthorized;
    }
  }


  public class MyAuthorization : Authorization
  {
    public override bool Authorize(string userName, string resource)
    {
      bool baseCondition = base.Authorize(userName, resource);

      bool additionalCondition = false;

      /*...*/

      return baseCondition && additionalCondition;
    }
  }
}
