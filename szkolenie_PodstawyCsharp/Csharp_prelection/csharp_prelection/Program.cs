﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Csharp_prelection
{
  class Program
  {
    static void Main(string[] args)
    {
      DailyReportDispatcher reportDispatcher = new DailyReportDispatcher();
      reportDispatcher.FormatReport();
      /// rezultat: Raport zostanie sformatowany jako zwykły tekst
      
      reportDispatcher = new PdfDailyReportDispatcher();
      reportDispatcher.FormatReport();
      /// rezultat: Raport w postaci dokumenty Pdf

    }
  }
}
