﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Csharp_prelection
{
  public class ReportDispatcher
  {
    public void Send()
    {
      
    }
  }

  public class DailyReportDispatcher : ReportDispatcher
  {
    public virtual void FormatReport()
    {
      /*...*/
    }
  }

  public class PdfDailyReportDispatcher : DailyReportDispatcher
  {
    /// zwraca raport w postaci Pdf
    public override void FormatReport()
    {
      /*...*/
    }
  }

}
