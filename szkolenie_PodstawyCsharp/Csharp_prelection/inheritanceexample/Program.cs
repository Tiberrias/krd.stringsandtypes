﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceExample
{
  class Program
  {
    static void Main(string[] args)
    {
    }
  }



  public class Manager
  {
    public string Name { get; set; }
    public DateTime DateofBirth { get; set; }
    public ushort Age()
    {
      return (ushort)(DateTime.Now.Year - this.DateofBirth.Year);
    }
    public Employee[] subordinates { get; set; }
  }

  public class Employee
  {
    public string Name { get; set; }
    public DateTime DateofBirth { get; set; }
    public ushort Age()
    {
      return (ushort)(DateTime.Now.Year - this.DateofBirth.Year);
    }
  }
}
