﻿using System;


namespace ReferenceTypExample
{
  class Program
  {
    static void Main(string[] args)
    {
      Circle c1, c2;

      c1 = new Circle();
      c1.Radius = 5;
      c2 = c1;

      Console.WriteLine("{0}   {1}", c1.Radius, c2.Radius);

      c2.Radius = 7;

      Console.WriteLine("{0}   {1}", c1.Radius, c2.Radius);
      Console.ReadKey();
    }
  }
  
  class Circle
  {
    public int Radius;

    public Circle()
    {
      
    }

    
  }
}
