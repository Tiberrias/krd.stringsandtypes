﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleApp
{
  public struct Book
  {
    public decimal Price;
    public string Title;
    public string Author;

    public Book(decimal price, string title, string author)
    {
      Price = price;
      Title = title;
      Author = author;
    }
  }
}
