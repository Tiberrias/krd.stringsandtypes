﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleApp
{
  using System;

  public class Program
  {
    public static void Main()
    {
      Console.WriteLine("Podaj swoje imie:");

      string imie = Console.ReadLine();
      Console.WriteLine("Twoje imie to: " + imie);

      Console.WriteLine("Wcisnij dowolny klawisz by zakonczyc program.");

      Console.WriteLine("Obiekt Widget: " + new Widget(2).ToString());
      Console.ReadKey();
    }
  }




  internal class Widget
  {
    private int _size;

    public Widget(int size)
    {
      this._size = size;
    }

    ~Widget()
    {
      this._size = 0;
    }

    private class InternalWidget
    {

    }
  }
}
