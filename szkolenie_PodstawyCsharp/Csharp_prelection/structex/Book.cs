﻿namespace StructEx
{
  public struct Book
  {
    public decimal Price;
    public string Title;
    public string Author;

    public Book(decimal price, string title, string author)
    {
      Price = price;
      Title = title;
      Author = author;
    }
  }
}
