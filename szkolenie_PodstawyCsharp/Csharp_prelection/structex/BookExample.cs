﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StructEx
{
  class BookExample
  {
    public string Title { get; set; }

    public BookExample(string title)
    {
      Title = title;
    }
  }
}
