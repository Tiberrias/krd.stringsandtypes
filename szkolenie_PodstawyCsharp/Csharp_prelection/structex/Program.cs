﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StructEx
{
  class Program
  {
    static void Main(string[] args)
    {
      Book book1;

      book1.Author = "Terry Pratchett";
      book1.Title = "Mort";
      book1.Price = 9.99m;

      Console.WriteLine(
        "Author: {0}, Title: {1}, Price: {2}",
        book1.Author,
        book1.Title,
        book1.Price);

      Console.WriteLine();
      Console.WriteLine("Press any key to exit.");
      Console.ReadKey();
    }
  }
}
